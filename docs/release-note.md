---
title: Release note
subtitle: Gazelle User Management
author: Valentin Lorand
toolversion: 3.2.1
releasedate: 2024-08-06
---
# Release note

## 3.2.1
_Release date: 2024-08-06_

__Bug__
* \[[GUM-284](https://gazelle.ihe.net/jira/browse/GUM-284)\] Problem of SMTP sender host configuration

## 3.2.0
_Release date: 2024-06-21_

__Improvement__
* \[[GUM-257](https://gazelle.ihe.net/jira/browse/GUM-257)\] Redundancy database possibility

## 3.1.1
_Release date: 2024-06-05_

__Bug__
* \[[GUM-243](https://gazelle.ihe.net/jira/browse/GUM-243)\] Configure other domain for SMTP sender mails
    
## 3.1.0
_Release date: 2024-05-13_

__Bug__
* \[[GUM-195](https://gazelle.ihe.net/jira/browse/GUM-195)\] Purge of inactivated user did not work.

__Task__
* \[[GUM-219](https://gazelle.ihe.net/jira/browse/GUM-219)\] [RAF] Common delegation code review

__Improvement__
* \[[GUM-210](https://gazelle.ihe.net/jira/browse/GUM-210)\] Update of Quarkus to 3.2.10.Final
* \[[GUM-220](https://gazelle.ihe.net/jira/browse/GUM-220)\] Easy access to Keycloak Admin Console for Gazelle admins

## 3.0.1
_Release date: 2024-04-19_

__Bug__
* \[[GUM-216](https://gazelle.ihe.net/jira/browse/GUM-216)\] Client registration does not update attributes at each restart
* \[[GUM-217](https://gazelle.ihe.net/jira/browse/GUM-217)\] Duplicate key value on consent

## 3.0.0
_Release date: 2024-04-05_

Step 3 of Gazelle User Management renovation (delegation of authentication).

__Story__
* \[[GUM-139](https://gazelle.ihe.net/jira/browse/GUM-139)\] [DELEG] Delegated user can access the external IDP.
* \[[GUM-140](https://gazelle.ihe.net/jira/browse/GUM-140)\] [DELEG] Authentication of a delegated user.
* \[[GUM-150](https://gazelle.ihe.net/jira/browse/GUM-150)\] [DELEG] Authentication of a new delegated user with existing delegated orga.
* \[[GUM-151](https://gazelle.ihe.net/jira/browse/GUM-151)\] [DELEG] Authentication of a new delegated user with new orga.
* \[[GUM-152](https://gazelle.ihe.net/jira/browse/GUM-152)\] [DELEG] Delegated user must accept Gazelle TOS
* \[[GUM-153](https://gazelle.ihe.net/jira/browse/GUM-153)\] [DELEG] Account collision of a new delegated user
* \[[GUM-154](https://gazelle.ihe.net/jira/browse/GUM-154)\] [DELEG] Organisation collision with delegated authn
* \[[GUM-155](https://gazelle.ihe.net/jira/browse/GUM-155)\] [DELEG] Update user attributes at delegation authentication.
* \[[GUM-156](https://gazelle.ihe.net/jira/browse/GUM-156)\] [DELEG] Update organization attributes at delegation authn
* \[[GUM-158](https://gazelle.ihe.net/jira/browse/GUM-158)\] [DELEG] Delegated user cannot login from GUM.
* \[[GUM-160](https://gazelle.ihe.net/jira/browse/GUM-160)\] [DELEG] Delegated user must not be able to reset their password in GUM.
* \[[GUM-166](https://gazelle.ihe.net/jira/browse/GUM-166)\] [DELEG] Logout of a delegated user
* \[[GUM-167](https://gazelle.ihe.net/jira/browse/GUM-167)\] [DELEG] Delegated session expiration
* \[[GUM-168](https://gazelle.ihe.net/jira/browse/GUM-168)\] [DELEG] Delegation session renewal
* \[[GUM-169](https://gazelle.ihe.net/jira/browse/GUM-169)\] [DELEG] Local users of a delegated-orga cannot login from GUM
* \[[GUM-170](https://gazelle.ihe.net/jira/browse/GUM-170)\] [DELEG] Block local account creation for a delegated-organization.

__Task__
* \[[GUM-171](https://gazelle.ihe.net/jira/browse/GUM-171)\] [DELEG] Support for PKCE
* \[[GUM-172](https://gazelle.ihe.net/jira/browse/GUM-172)\] [DELEG] Support for Client Assertion
* \[[GUM-173](https://gazelle.ihe.net/jira/browse/GUM-173)\] [DELEG] Support ID Token signed with client_secret
* \[[GUM-174](https://gazelle.ihe.net/jira/browse/GUM-174)\] [DELEG] Support Realm updates with custom configs
* \[[GUM-175](https://gazelle.ihe.net/jira/browse/GUM-175)\] [DELEG] Packaging and extensions deployment
* \[[GUM-196](https://gazelle.ihe.net/jira/browse/GUM-196)\] Secure rest endpoint
* \[[GUM-207](https://gazelle.ihe.net/jira/browse/GUM-207)\] Add application licensing

__Improvement__
* \[[GUM-191](https://gazelle.ihe.net/jira/browse/GUM-191)\] [DELEG] Authorize aliases in emails gum-backend + gum-ui

## 2.0.1
_Release date: 2024-03-07_

__Bug__
* \[[GUM-193](https://gazelle.ihe.net/jira/browse/GUM-193)\] Missing error log in case of Mail failure or unexpected failure at user registration.

__Task__
* \[[GUM-187](https://gazelle.ihe.net/jira/browse/GUM-187)\] [UI] Add password requirements

## 2.0.0
_Release date: 2024-01-23_

Step 2 of Gazelle User Management renovation (migrate users in GUM microservice)

__Story__
* \[[GUM-46](https://gazelle.ihe.net/jira/browse/GUM-46)\] User submits account creation request
* \[[GUM-56](https://gazelle.ihe.net/jira/browse/GUM-56)\] Improve password security
* \[[GUM-60](https://gazelle.ihe.net/jira/browse/GUM-60)\] Remove username display in gazelle apps
* \[[GUM-78](https://gazelle.ihe.net/jira/browse/GUM-78)\] Create webservice to search/edit users in GUM from TM
* \[[GUM-90](https://gazelle.ihe.net/jira/browse/GUM-90)\] Options to prohibit organisation creation / user registration
* \[[GUM-94](https://gazelle.ihe.net/jira/browse/GUM-94)\] Collect user consents
* \[[GUM-96](https://gazelle.ihe.net/jira/browse/GUM-96)\] Purge users never activated

__Task__
* \[[GUM-51](https://gazelle.ihe.net/jira/browse/GUM-51)\] Init user preferences through webservice
* \[[GUM-55](https://gazelle.ihe.net/jira/browse/GUM-55)\] Script to migrate users
* \[[GUM-57](https://gazelle.ihe.net/jira/browse/GUM-57)\] Industrialisation of M2M authentication
* \[[GUM-65](https://gazelle.ihe.net/jira/browse/GUM-65)\] Init user-management-quarkus module
* \[[GUM-67](https://gazelle.ihe.net/jira/browse/GUM-67)\] Writing interface GUM specifications
* \[[GUM-76](https://gazelle.ihe.net/jira/browse/GUM-76)\] Client to perform actions on organisations in TM
* \[[GUM-88](https://gazelle.ihe.net/jira/browse/GUM-88)\] Integrate M2M to GUM and TM
* \[[GUM-99](https://gazelle.ihe.net/jira/browse/GUM-99)\] Customizable Logo in login form
* \[[GUM-100](https://gazelle.ihe.net/jira/browse/GUM-100)\] Integrate ServiceMetadata with scopes
* \[[GUM-104](https://gazelle.ihe.net/jira/browse/GUM-104)\] Cache access tokens to limit calls to Keycloak
* \[[GUM-146](https://gazelle.ihe.net/jira/browse/GUM-146)\] Always Lowercase user ids and emails for insertions

## 1.0.1
_Release date: 2023-09-14_

__Bug__
* \[[GUM-93](https://gazelle.ihe.net/jira/browse/GUM-93)\] User cannot login if two users have the same "lower case"

## 1.0.0
_Release date: 2023-07-11_

Step 1 of Gazelle User Management renovation (add Keycloak as SSO).

__Bug__
* \[[GUM-49](https://gazelle.ihe.net/jira/browse/GUM-49)\] Keycloak can't manage groups with spaces

__Story__
* \[[GUM-35](https://gazelle.ihe.net/jira/browse/GUM-35)\] User authenticate himself (Part 1)
* \[[GUM-36](https://gazelle.ihe.net/jira/browse/GUM-36)\] Unsuccessful login (Notify user)
* \[[GUM-37](https://gazelle.ihe.net/jira/browse/GUM-37)\] Unsuccessful login (Blocks accounts)
* \[[GUM-38](https://gazelle.ihe.net/jira/browse/GUM-38)\] Redirect user to request page after login
* \[[GUM-43](https://gazelle.ihe.net/jira/browse/GUM-43)\] User reset his password
* \[[GUM-48](https://gazelle.ihe.net/jira/browse/GUM-48)\] Create account link redirect to TM account creation.
* \[[GUM-50](https://gazelle.ihe.net/jira/browse/GUM-50)\] Manage internationalization for emails

__Task__
* \[[GUM-33](https://gazelle.ihe.net/jira/browse/GUM-33)\] Upgrade Gazelle keycloak to V21
* \[[GUM-34](https://gazelle.ihe.net/jira/browse/GUM-34)\] Setup CI with Integration tests
* \[[GUM-41](https://gazelle.ihe.net/jira/browse/GUM-41)\] Log out
* \[[GUM-61](https://gazelle.ihe.net/jira/browse/GUM-61)\] [GUM] Register gazelle app in Keycloak at startup
* \[[GUM-62](https://gazelle.ihe.net/jira/browse/GUM-62)\] Create end 2 end tests with Cypress
